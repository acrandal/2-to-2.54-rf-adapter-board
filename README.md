# 2 to 2.54 RF Adapter Board

Many RF modules sold today use a 2mm pitch for their holes. I asked the FIZ at WSU to design an adapter board to bring it out to 2.54mm pitch to work with most breadboards available.